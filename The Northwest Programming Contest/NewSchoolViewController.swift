//
//  NewSchoolViewController.swift
//  The Northwest Programming Contest
//
//  Created by Rahul Reddy on 14/03/19.
//  Copyright © 2019 Rahul  Reddy. All rights reserved.
//

import UIKit

class NewSchoolViewController: UIViewController {

    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func done(_ sender: Any) {
        let schoolName = schoolNameTF.text!
        let coachName = coachNameTF.text!
        if schoolNameTF.text!.count != 0 && coachNameTF.text!.count != 0{
            
            Schools.shared.saveSchool(name: schoolName, coach: coachName)
            self.dismiss(animated: true, completion: nil)
        }else if schoolNameTF.text!.count == 0 {
            displayMessage(fieldName: "School")
        }else if coachNameTF.text!.count == 0 {
            displayMessage(fieldName: "Coach")
        }
    }
    @IBOutlet weak var coachNameTF: UITextField!
    @IBOutlet weak var schoolNameTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func displayMessage(fieldName: String){
        let alert = UIAlertController(title: "Error",
                                      message: "Please enter a valid value in \(fieldName) text field.",
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
