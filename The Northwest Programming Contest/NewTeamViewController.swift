//
//  NewTeamViewController.swift
//  The Northwest Programming Contest
//
//  Created by Rahul Reddy  on 14/03/19.
//  Copyright © 2019 Rahul  Reddy. All rights reserved.
//

import UIKit

class NewTeamViewController: UIViewController {

    
    
    @IBOutlet weak var student2TF: UITextField!
    
    @IBOutlet weak var student1TF: UITextField!
    @IBOutlet weak var student0TF: UITextField!
    
    @IBOutlet weak var nameTF: UITextField!
    var school:School!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func doneBTN(_ sender: Any) {
        let name = nameTF.text!
        let student0Name = student0TF.text!
        let student1Name = student1TF.text!
        let student2Name = student2TF.text!
        if nameTF.text!.count != 0 && student0TF.text!.count != 0 && student1TF.text!.count != 0 && student2TF.text!.count != 0 {
            Schools.shared.saveTeamForSelectedSchool(school: school, team: Team(name: name, students: [student0Name, student1Name, student2Name]))

        
        self.dismiss(animated: true, completion: nil)
        }
    else if nameTF.text!.count != 0 {
    displayMessage(fieldName: "Name")
    } else if student0TF.text!.count != 0 {
    displayMessage(fieldName: "Student 0")
    } else if student1TF.text!.count != 0 {
    displayMessage(fieldName: "Student 1")
    } else if student2TF.text!.count != 0 {
    displayMessage(fieldName: "Student 2")
    }
}
    func displayMessage(fieldName: String){
        let alert = UIAlertController(title: "Error",
                                      message: "Please enter a valid value in \(fieldName) text field.",
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func cancelBTN(_ sender: Any) {
                self.dismiss(animated: true, completion: nil)
    }
}
